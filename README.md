# HN NEWS!

First to install node_modules use
```
npm i
```
To start the whole project use:
```
docker-compose up
```
To start only the server use:
```
npm start api
```
To only start the client use:
```
npm start reign-client
```
After the project is up, here is server's url: http://localhost:5000/api/news and client's url: http://localhost:4900

### Enjoy! 
